import sys
sys.path.append('.')

import time
from .FbClient import FbClient
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from random import randint

def find_element_by_css_selector(driver, css_selector, required=True):
  try:
    return driver.find_element_by_css_selector(css_selector)
  except:
    if required is False:
      return None
    else:
      raise Exception(f'Element {css_selector} is required, but not found. Please try again.')

def wait_element_by_css_selector(driver, css_selector, required=True):
  try:
    ret = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CSS_SELECTOR, css_selector)))
    return ret
  except:
    if required is False:
      return None
    else:
      raise Exception(f'Element {css_selector} is required, but not found. Please try again.')

def run_one(cookies, proxy, url='', chrome_driver_execute_path=None):
  try:
    fbClient = FbClient(start_page='https://www.facebook.com', url=url, cookies=cookies, proxy=proxy, chrome_driver_execute_path=chrome_driver_execute_path)
    driver = fbClient.open()

    high_school_url = 'https://m.facebook.com/profile/edit/infotab/section/forms/?info_surface=intro_card&life_event_surface=mtouch_profile&section=education&experience_type=2003&cb=1610153350&__xts__%5B0%5D=69.%7B%22section_type%22%3A%22education%22%7D'
    driver.get(high_school_url)
    high_school_values = 'THPT Phong Phú|THPT Nguyễn Thị Diệu|THPT Nguyễn Khuyến Q10|THPT Nguyễn Hiền|THPT Phan Đăng Lưu|THPT Hùng Vương|THPT Chuyên Trần Đại Nghĩa|THPT Phước Kiển|THPT Trần Văn Giàu|THPT Trần Khai Nguyên'.split('|')
    input = wait_element_by_css_selector(driver, css_selector='form header+div+div')
    input.click()
    input = wait_element_by_css_selector(driver, css_selector='form header+div input[type="text"]')
    input.clear()
    input.send_keys(high_school_values[randint(0, len(high_school_values) - 1)])
    input = wait_element_by_css_selector(driver, css_selector='form button[type="submit"]')
    input.click()

    college_url = 'https://m.facebook.com/profile/edit/infotab/section/forms/?info_surface=intro_card&life_event_surface=mtouch_profile&section=education&experience_type=2004&cb=1610162306&__xts__%5B0%5D=69.%7B%22section_type%22%3A%22education%22%7D'
    driver.get(college_url)
    college_values = 'Đại học Quốc Gia TP.HCM|HUTECH - Đại học Công nghệ Tp.HCM|HUFI - Đại học Công nghiệp Thực phẩm Tp.HCM|UEF - Đại học Kinh tế Tài chính TP.HCM|Trường Đại học Khoa học Tự nhiên, Đại học Quốc gia TP.HCM|Đại học Mở Tp.HCM'.split('|')
    input = wait_element_by_css_selector(driver, css_selector='form header+div+div')
    input.click()
    input = wait_element_by_css_selector(driver, css_selector='form header+div input[type="text"]')
    input.clear()
    input.send_keys(college_values[randint(0, len(college_values) - 1)])
    input = wait_element_by_css_selector(driver, css_selector='form button[type="submit"]')
    input.click()

    living_url = 'https://m.facebook.com/profile/edit/infotab/section/forms/?life_event_surface=mtouch_profile&section=living'
    driver.get(living_url)
    current_city_values = 'Thành phố Hồ Chí Minh|An Giang|Đồng Tháp|Mỹ Tho|Tiền Giang|Gia Lai'.split('|')
    hometown_values = ['Thành phố Hồ Chí Minh']
    input = find_element_by_css_selector(driver, css_selector='form[method="post"]>div[data-sigil="profile-card edit-info-card"] input[type="text"]')
    input.click()
    input.clear()
    input.send_keys(current_city_values[randint(0, len(current_city_values) - 1)])
    time.sleep(1)
    input.send_keys(Keys.DOWN, Keys.ENTER)
    input = find_element_by_css_selector(driver, css_selector='form[method="post"]>div[data-sigil="profile-card edit-info-card"]+div[data-sigil="profile-card edit-info-card"] input[type="text"]')
    input.click()
    input.clear()
    input.send_keys(hometown_values[randint(0, len(hometown_values) - 1)])
    time.sleep(1)
    input.send_keys(Keys.DOWN, Keys.ENTER)
    input = wait_element_by_css_selector(driver, css_selector='form button[type="submit"]')
    input.click()

  except Exception as e:
    print(e)
  finally:
    time.sleep(120)
    driver.close()