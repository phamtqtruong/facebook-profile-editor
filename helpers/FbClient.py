from selenium import webdriver
from selenium.webdriver.chrome.options import Options
# from fake_useragent import UserAgent

import time

class FbClient:
  def __init__(self, start_page, cookies, url, proxy=None, chrome_driver_execute_path=None):
    options = Options()
    # ua = UserAgent()
    # userAgent = ua.random
    options.add_argument('--user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1')
    if proxy is not None:
      options.add_argument(f'--proxy-server={proxy}')
    options.add_argument('--headless')
    options.add_argument('--window-size=1366,768')
    if cookies is None:
      raise Exception('Need any FB account cookies')
    driver = webdriver.Chrome(chrome_options=options)
    if chrome_driver_execute_path is not None:
      driver = webdriver.Chrome(chrome_options=options, executable_path=chrome_driver_execute_path)
    driver.get(start_page)
    for cookie in cookies.strip().split(';'):
      cookie_pair = cookie.split('=')
      cookie_name = cookie_pair[0].strip()
      cookie_value = ''
      try:
        cookie_value = cookie_pair[1].strip()
      except:
        pass
      driver.add_cookie({'name': cookie_name, 'value': cookie_value})
    self.url = url.strip()
    self.start_page = start_page.strip()
    self.driver = driver
  def open(self):
    self.driver.get(self.start_page)
    time.sleep(1)
    return self.driver